var fs = require('fs')
var Hose = require('flowr-hose')
var wordpress = require( "wordpress" )

var hose = new Hose({
  name: 'wp',
  port: process.env.PORT || 3000,
  kue: {
    prefix: 'q',
    redis: process.env.KUE
  }
});

console.log("TTN.WP is running...")

var clients = []

hose.process('post', process.env.CONCURRENCY || 1, function(job, done){
	post(job.data, function(err, url){
		console.log(err)
		done(null, url)
	})
})

hose.process('getById', process.env.CONCURRENCY || 1, function(job, done){
  get(job.data, function(err, data){
    console.log(err)
    done(null, data)
  })
})

function getClient(conf){
  var client;

  if(!clients[conf.url]){
    console.log("creating new wp client")
    console.log(conf)
    client = wordpress.createClient(conf)
    clients[conf.url] = client
  } else {
    console.log("using cached wp client")
    client = clients[conf.url]
  }
  return client
}


function get(params, cb) {
  console.log("PARAMS: ",params)
  getClient(params.wp).getPost(params.postId, function(err, post){
    console.log(err)
    console.log(post)
    cb(null, post)
  })
}

function post(params, cb){
  getClient(params.wp)
  .newPost(params.post, function(err,res){
      if(err){
            console.log(err);
      }
      console.log(err)
      console.log(res)
      cb(null, res)
  });
}
